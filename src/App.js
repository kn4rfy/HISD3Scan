import React from 'react'
import { AsyncStorage } from 'react-native'
import { persistStore, autoRehydrate } from 'redux-persist'

import dva from './utils/dva'
import Router from './router'

import authModel from './models/auth'
import routerModel from './models/router'

import { LocaleProvider } from 'antd-mobile'

import enUS from 'antd-mobile/lib/locale-provider/en_US'

const app = dva({
	initialState: {},
	models: [authModel, routerModel],
	// extraEnhancers: [autoRehydrate()],
	onError(e) {
		console.log('onError', e)
	},
})

const App = app.start(
	<LocaleProvider locale={enUS}>
		<Router />
	</LocaleProvider>
)

export default App
persistStore(app.getStore(), {
	storage: AsyncStorage,
	blacklist: ['router'],
})
