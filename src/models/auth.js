import { Toast } from 'antd-mobile'
import { createAction, NavigationActions } from '../utils'
import * as authService from '../services/auth'
import * as rest from '../utils/RestClient'

export default {
	namespace: 'auth',
	state: {
		loading: false,
		isAuthenticated: false,
		authErrorMessage: null,
		isNotActive: false,
		account: null,
		logoutSuccess: false,
		fromStart: false,
		disableSaveEditUser: false,
		activeRecord: {},
	},
	reducers: {
		accountReceived(state, { account }) {
			return {
				...state,
				isAuthenticated: true,
				authErrorMessage: null,
				account,
				logoutSuccess: false,
				disableSaveEditUser: false,
			}
		},
		loginStart(state, { payload }) {
			return { ...state, ...payload, loading: true }
		},
		loginEnd(state, { payload }) {
			return { ...state, ...payload, loading: false }
		},
		logoutStart(state, { payload }) {
			return { ...state, ...payload, loading: true }
		},
		logoutEnd(state, { payload }) {
			return { ...state, ...payload, loading: false }
		},
	},
	effects: {
		*login(payload, { call, put }) {
			try {
				yield put(createAction('loginStart')())
				const login = yield call(authService.login, payload)
				if (login) {
					yield put({ type: 'loginSuccess' })
				}
				yield put(createAction('loginEnd')({ login }))
			} catch (error) {
				if (error.response) {
					yield Toast.fail(error.response.data.message, 1)
				} else {
					// eslint-disable-next-line no-underscore-dangle
					yield Toast.fail(error.request._response, 1)
				}
				yield put(createAction('loginEnd')())
			}
		},
		*loginSuccess(payload, { call, put }) {
			try {
				const account = yield call(authService.login, payload)
				yield put({
					type: 'accountReceived',
					account: account.data,
				})
			} catch (error) {
				console.log('error login', error)
			}
			yield put(
				NavigationActions.reset({
					index: 0,
					actions: [NavigationActions.navigate({ routeName: 'Main' })],
				})
			)
		},
		*logout({ payload }, { call, put }) {
			yield put(createAction('logoutStart')())
			const logout = yield call(authService.logout, payload)
			if (logout) {
				yield put(
					NavigationActions.reset({
						index: 0,
						actions: [NavigationActions.navigate({ routeName: 'Auth' })],
					})
				)
			}
			yield put(createAction('logoutEnd')({ logout }))
		},
	},
}
