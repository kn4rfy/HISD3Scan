import { Toast } from 'antd-mobile'
import { createAction, NavigationActions } from '../utils'
import * as authService from '../services/auth'
import * as rest from '../utils/RestClient'

import * as Config from '../config/Config'

export default {
	namespace: 'healthchecks',
	state: {
		status: null,
	},
	reducers: {
		pingSuccess() {
			return { status: 'OK' }
		},
		pingFailed() {
			return { status: 'Failed' }
		},
	},
	effects: {
		ping: [
			function*({ call, put }) {
				try {
					yield call(rest.get(`${Config.getHost()}/api/public/ping`))
					yield put(createAction('pingSuccess')())
				} catch (error) {
					yield put(createAction('pingFailed')())
				}
			},
			{ type: 'takeLatest' },
		],
	},
}
