import React, { PureComponent } from 'react'
import { BackHandler, Animated, Easing, Text } from 'react-native'
import { StackNavigator, addNavigationHelpers, NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'

import { Auth } from './components'
import { QrSignatureCamera, QrSignatureConfirm } from './components/signature'
import { Dashboard } from './containers'
import { TitleBar } from './layout'

const MainNavigator = StackNavigator(
	{
		Dashboard: {
			screen: Dashboard,
			navigationOptions: ({ navigation }) => TitleBar.standard(navigation, 'HISD3', false),
		},
	},
	{
		headerMode: 'float',
	}
)

const ModalNavigator = StackNavigator(
	{
		Auth: { screen: Auth },
		Main: {
			screen: ({ navigation }) => (
				<MainNavigator screenProps={{ rootNavigation: navigation }} />
			),
		},
		QrSignatureCamera: { screen: QrSignatureCamera },
		QrSignatureConfirm: { screen: QrSignatureConfirm },
	},
	{
		headerMode: 'none',
		mode: 'modal',
		navigationOptions: {
			gesturesEnabled: false,
		},
		transitionConfig: () => ({
			transitionSpec: {
				duration: 300,
				easing: Easing.out(Easing.poly(4)),
				timing: Animated.timing,
			},
			screenInterpolator: sceneProps => {
				const { layout, position, scene } = sceneProps
				const { index } = scene

				const height = layout.initHeight
				const translateY = position.interpolate({
					inputRange: [index - 1, index, index + 1],
					outputRange: [height, 0, 0],
				})

				const opacity = position.interpolate({
					inputRange: [index - 1, index - 0.99, index],
					outputRange: [0, 1, 1],
				})

				return { opacity, transform: [{ translateY }] }
			},
		}),
	}
)

function getCurrentScreen(navigationState) {
	if (!navigationState) {
		return null
	}
	const route = navigationState.routes[navigationState.index]
	if (route.routes) {
		return getCurrentScreen(route)
	}
	return route.routeName
}

@connect(({ router }) => ({ router }))
class Router extends PureComponent {
	componentWillMount() {
		BackHandler.addEventListener('hardwareBackPress', this.backHandle)
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backHandle)
	}

	backHandle = () => {
		const currentScreen = getCurrentScreen(this.props.router)
		if (currentScreen === 'Auth') {
			return true
		}
		if (currentScreen !== 'Dashboard') {
			this.props.dispatch(NavigationActions.back())
			return true
		}
		return false
	}

	render() {
		const { dispatch, router } = this.props
		const navigation = addNavigationHelpers({ dispatch, state: router })
		return <ModalNavigator navigation={navigation} />
	}
}

export function routerReducer(state, action = {}) {
	return ModalNavigator.router.getStateForAction(action, state)
}

export default Router
