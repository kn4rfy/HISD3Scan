import React, { PureComponent } from 'react'
import { Alert, TouchableOpacity } from 'react-native'
import { Icon, WingBlank, WhiteSpace } from 'antd-mobile'
import { connect } from 'react-redux'
import { NavigationActions, createAction } from '../utils'

const backAction = navigation => () => navigation.dispatch(NavigationActions.back())

const navigateTo = (navigation, route, params = {}) => {
	navigation.dispatch(NavigationActions.navigate({ routeName: route, params }))
}

const resetTo = (navigation, route) => {
	navigation.dispatch(
		NavigationActions.reset({
			index: 0,
			actions: [NavigationActions.navigate({ routeName: route })],
		})
	)
}

const resetToDashboard = navigation => {
	navigation.dispatch(
		NavigationActions.reset({
			index: 1,
			actions: [
				NavigationActions.navigate({ routeName: 'ClinicList' }),
				NavigationActions.navigate({ routeName: 'Dashboard' }),
			],
		})
	)
}

const drawerAction = (navigation, route) => () => {
	navigation.navigate(route)
}

@connect(({ auth }) => ({ ...auth }))
class LogoutButton extends PureComponent {
	onLogout = () => {
		Alert.alert('Confirmation', 'Please Confirm to Logout', [
			{
				text: 'Confirm',
				onPress: () => this.props.dispatch(createAction('auth/logout')()),
			},
			{ text: 'Cancel', onPress: () => {}, style: 'cancel' },
		])
	}

	render() {
		return (
			<TouchableOpacity onPress={this.onLogout}>
				<WingBlank>
					<Icon type={'\ue65a'} color={'#FFFFFF'} size={28} />
				</WingBlank>
			</TouchableOpacity>
		)
	}
}

export { backAction, navigateTo, resetTo, resetToDashboard, LogoutButton, drawerAction }
