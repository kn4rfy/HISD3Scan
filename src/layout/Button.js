import React, { PureComponent } from 'react'
import { Text, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { WingBlank, WhiteSpace } from 'antd-mobile'

export default class Button extends PureComponent {
	render() {
		const borderRadius = this.props.removeRadius ? 0 : 15

		return (
			<TouchableOpacity
				activeOpacity={0.8}
				onPress={this.props.onPress}
				style={this.props.style}
			>
				<WingBlank>
					<LinearGradient
						colors={['#0678a0', '#04526d']}
						start={{ x: 0, y: 1 }}
						style={{
							backgroundColor: 'transparent',
							alignItems: 'center',
							justifyContent: 'center',
							height: 47,
							paddingHorizontal: 15,
							borderRadius: 25,
						}}
					>
						<Text style={{ fontSize: 17, fontWeight: 'bold', color: '#FFFFFF' }}>
							{this.props.title}
						</Text>
					</LinearGradient>
				</WingBlank>
				<WhiteSpace size={'xl'} />
			</TouchableOpacity>
		)
	}
}
