import React from 'react'
import { View, TouchableOpacity, Image, Text } from 'react-native'
import { Icon, WingBlank, WhiteSpace } from 'antd-mobile'
import LinearGradient from 'react-native-linear-gradient'

import { RouteActions } from './'

export const standard = (navigation, title = '', showBackButton = true) => {
	const header = (
		<LinearGradient
			colors={['#0678a0', '#04526d']}
			start={{ x: 0, y: 1 }}
			style={{
				backgroundColor: 'transparent',
			}}
		>
			<View style={{ flexDirection: 'row', height: 47, marginTop: 20 }}>
				{showBackButton ? (
					<View style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
						<WhiteSpace />
						<TouchableOpacity onPress={RouteActions.backAction(navigation)}>
							<WingBlank>
								<Icon type={'\ue61c'} color={'#FFFFFF'} size={28} />
							</WingBlank>
						</TouchableOpacity>
						<WhiteSpace />
					</View>
				) : (
					<View style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
						<WhiteSpace />
						<TouchableOpacity>
							<WingBlank>
								<Icon type={'\ue61c'} color={'transparent'} size={28} />
							</WingBlank>
						</TouchableOpacity>
						<WhiteSpace />
					</View>
				)}
				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
					<WhiteSpace />
					<Text style={{ fontSize: 22, fontWeight: 'bold', color: '#FFFFFF' }}>
						{title}
					</Text>
					<WhiteSpace />
				</View>
				<View style={{ alignItems: 'flex-end', justifyContent: 'center' }}>
					<WhiteSpace />
					<RouteActions.LogoutButton />
					<WhiteSpace />
				</View>
			</View>
		</LinearGradient>
	)

	return {
		header,
		gesturesEnabled: false,
	}
}
