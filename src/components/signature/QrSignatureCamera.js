import React, { Component } from 'react'
import {
	View,
	Alert,
	Platform,
	NativeModules,
	Linking,
	PermissionsAndroid,
	StatusBar,
	Text,
} from 'react-native'
import { Icon } from 'antd-mobile'
import { NavigationActions } from 'react-navigation'
import Camera from 'react-native-camera'
import Device from 'react-native-device-detection'

import { connect } from 'react-redux'

import { computeSize } from '../../utils/DeviceRatio'
import * as Config from '../../config/Config'

const barcodeTypes = Platform.select({
	ios: ['org.iso.QRCode'],
	android: ['qr'],
})

@connect()
export default class QrSignatureCamera extends Component {
	constructor(props) {
		super(props)

		this.state = {
			deviceAuthorization: false,
			camera: {
				onBarCodeRead: this.onBarCodeRead,
			},
		}
	}

	componentWillMount() {
		if (
			Config.getHost() === 'http://undefined' ||
			Config.getHost() === 'http://null' ||
			Config.getHost() === 'https://undefined' ||
			Config.getHost() === 'https://null'
		) {
			Alert.alert('Error Config', 'Please specify API Host at Settings', [
				{
					text: 'OK',
					onPress: () => {
						this.props.navigation.dispatch(
							NavigationActions.navigate({ routeName: 'settings' })
						)
					},
				},
			])
		}
	}

	componentDidMount() {
		if (Device.isIos) {
			NativeModules.CameraManager.checkVideoAuthorizationStatus()
				.then(result => {
					this.setState({ deviceAuthorization: result }, () => {
						if (!result) {
							Linking.canOpenURL('app-settings:')
								.then(supported => {
									if (supported) {
										Alert.alert(
											'Camera was not granted access',
											'Please allow camera access in settings',
											[
												{
													text: 'Settings',
													onPress: () => {
														Linking.openURL('app-settings:')
													},
												},
											]
										)
									}
								})
								.catch(err => {
									if (__DEV__) {
										console.log('error Linking.canOpenURL', err)
									}
								})
						}
					})
				})
				.catch(err => {
					if (__DEV__) {
						console.log(
							'error NativeModules.CameraManager.checkDeviceAuthorizationStatus',
							err
						)
					}
				})
		}
		if (Device.isAndroid) {
			this.requestCameraPermission()
		}
	}

	async requestCameraPermission() {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.CAMERA,
				{
					title: 'HISD3 Camera Permission',
					message: 'HISD3 needs access to your camera ' + 'so you can scan QR.',
				}
			)

			if (typeof granted === 'boolean') {
				this.setState({ deviceAuthorization: granted })
			} else if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				this.setState({ deviceAuthorization: true })
			} else {
				this.setState({ deviceAuthorization: false })
			}
		} catch (err) {
			if (__DEV__) {
				console.log('error PermissionsAndroid.request', err)
			}
		}
	}

	cancel = () => {
		this.props.navigation.goBack()
	}

	onBarCodeRead = result => {
		const data = JSON.parse(result.data)
		this.setState({ camera: { onBarCodeRead: null } }, () => {
			this.props.navigation.dispatch(
				NavigationActions.navigate({ routeName: 'QrSignatureConfirm', params: data })
			)
			setTimeout(() => {
				this.setState({ camera: { onBarCodeRead: this.onBarCodeRead } })
			}, 3000)
		})
	}

	render() {
		return (
			<View style={{ flex: 1, backgroundColor: '#000' }}>
				<StatusBar animated hidden />

				{this.state.deviceAuthorization ? (
					<Camera
						ref={cam => {
							this.camera = cam
						}}
						audio={false}
						onBarCodeRead={this.state.camera.onBarCodeRead}
						barCodeTypes={barcodeTypes}
						style={{ flex: 1 }}
						aspect={Camera.constants.Aspect.fill}
					>
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
							<Text h1 style={{ color: 'white', marginBottom: computeSize(20) }}>
								Scan QR Code
							</Text>
							<View
								style={{
									width: computeSize(650),
									height: computeSize(650),
									borderWidth: 5,
									borderColor: 'white',
								}}
							/>
						</View>
					</Camera>
				) : (
					<View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
						<View
							style={{
								flex: 1,
								backgroundColor: 'transparent',
								justifyContent: 'center',
								alignItems: 'center',
							}}
						>
							<Text h1 style={{ color: 'white', marginBottom: computeSize(20) }}>
								Camera was not granted access.
							</Text>
							<Text h2 style={{ color: 'white', marginBottom: computeSize(20) }}>
								Please allow camera access in app setting
							</Text>
						</View>
					</View>
				)}

				<View style={{ flexDirection: 'row', marginVertical: computeSize(20) }}>
					<View style={{ alignItems: 'center', flex: 1 }}>
						<Icon type={'cross'} color={'white'} size={80} onPress={this.cancel} />
					</View>
				</View>
			</View>
		)
	}
}
