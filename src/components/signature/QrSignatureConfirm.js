import React, { Component } from 'react'
import { View, Modal, Alert, Text, TouchableOpacity } from 'react-native'
import SignatureCapture from 'react-native-signature-capture'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'

import { Button } from '../../layout'

@connect()
export default class QrSignatureConfirm extends Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() {
		this.props.dispatch(createAction('healthchecks/ping')())
	}

	saveSign = () => {
		this.signatureComponentRef.saveImage()
	}

	resetSign = () => {
		this.signatureComponentRef.resetImage()
	}

	cancelSign = () => {
		this.props.navigation.dispatch(NavigationActions.back())
	}

	_onSaveEvent = result => {
		const payload = {
			entityName: this.props.navigation.state.params.entityName,
			columnName: this.props.navigation.state.params.fieldName,
			rowId: this.props.navigation.state.params.rowId,
			base64Str: result.encoded,
		}

		this.props.pdscActions.saveSignature(payload, (result, message) => {
			if (result) {
				Alert.alert('Success', message, [
					{
						text: 'OK',
						onPress: () => {
							this.props.navigation.dispatch(NavigationActions.back())
						},
					},
				])
			} else {
				Alert.alert('Error', message, [
					{
						text: 'OK',
						onPress: () => {},
					},
				])
			}
		})
	}

	_onDragEvent = result => {
		// This callback will be called when the user enters signature
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				<View style={{ flex: 0.2 }} />
				<View style={{ flex: 0.5, padding: 10 }}>
					<SignatureCapture
						ref={ref => (this.signatureComponentRef = ref)}
						onSaveEvent={this._onSaveEvent}
						onDragEvent={this._onDragEvent}
						saveImageFileInExtStorage={false}
						showNativeButtons={false}
						viewMode={'landscape'}
						style={{ flex: 1, border: 1 }}
					/>
				</View>
				<View style={{ flex: 0.3 }}>
					<Text style={{ textAlign: 'center' }}>
						{this.props.navigation.state.params.signatoryName != 'null'
							? this.props.navigation.state.params.signatoryName
							: null}
					</Text>
					<Text style={{ textAlign: 'center' }}>
						{this.props.navigation.state.params.signatory != 'null'
							? this.props.navigation.state.params.signatory
							: null}
					</Text>
				</View>
				<View style={{ flexDirection: 'row', marginVertical: 20 }}>
					<View style={{ flex: 0.3 }}>
						<Button title={'Cancel'} onPress={() => this.cancelSign()} />
					</View>

					<View style={{ flex: 0.3 }}>
						<Button title={'Reset'} onPress={() => this.cancelSign()} />
					</View>

					<View style={{ flex: 0.4 }}>
						<Button title={'Submit'} onPress={() => this.cancelSign()} />
					</View>
				</View>
			</View>
		)
	}
}
