import React, { Component } from 'react'
import { View, Text, AsyncStorage, Image } from 'react-native'
import { Flex, WingBlank, WhiteSpace, InputItem, ActivityIndicator } from 'antd-mobile'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'

import { createAction } from '../utils/index'
import { setHost } from '../config/Config'
import { Button } from '../layout'

@connect(({ auth }) => ({ ...auth }))
export default class Auth extends Component {
	constructor(props) {
		super(props)

		this.state = {
			j_username: 'demogenome',
			j_password: 'password',
			host: 'localhost:8080',
		}
	}

	onSubmit = () => {
		setHost(this.state.host)
		AsyncStorage.setItem(
			'Authorization',
			JSON.stringify({
				username: this.state.j_username,
				password: this.state.j_password,
			})
		)
		this.props.dispatch(createAction('auth/login')(this.state))
	}

	onFieldUpdate = (field, value) => {
		this.setState({ [field]: value })
	}

	onFieldUpdateHost = (field, value) => {
		this.setState({ [field]: value }, setHost(this.state.host))
	}

	render() {
		const { loading } = this.props
		return (
			<KeyboardAwareScrollView
				style={{ backgroundColor: '#fff' }}
				contentContainerStyle={{
					flex: 1,
					justifyContent: 'center',
				}}
			>
				<View
					style={{ flex: 0.8, justifyContent: 'center', backgroundColor: 'transparent' }}
				>
					<WingBlank>
						<View
							style={{
								backgroundColor: 'white',
								opacity: 0.5,
								paddingTop: 10,
								borderRadius: 10,
							}}
						>
							<WingBlank>
								<Text style={{ fontSize: 20, fontWeight: 'bold' }}>Username</Text>
							</WingBlank>
							<InputItem
								autoCorrect={false}
								autoCapitalize={'none'}
								value={this.state.j_username}
								onChange={value => this.onFieldUpdate('j_username', value)}
								onSubmitEditing={() => this.password.inputRef.inputRef.focus()}
								blurOnSubmit={false}
								returnKeyType={'next'}
								placeholder={'Enter username'}
							/>

							<WingBlank>
								<Text style={{ fontSize: 20, fontWeight: 'bold' }}>Password</Text>
							</WingBlank>
							<InputItem
								ref={c => {
									this.password = c
								}}
								type={'password'}
								value={this.state.j_password}
								onChange={value => this.onFieldUpdate('j_password', value)}
								onSubmitEditing={this.onSubmit}
								placeholder={'Enter password'}
								returnKeyType={'go'}
							/>
						</View>
					</WingBlank>
				</View>

				<ActivityIndicator toast animating={loading} />

				<WingBlank>
					<WhiteSpace size={'lg'} />
					<Flex>
						<Flex.Item>
							<Button title={'Login'} onPress={this.onSubmit} />
						</Flex.Item>
					</Flex>
					<WhiteSpace size={'lg'} />
				</WingBlank>
			</KeyboardAwareScrollView>
		)
	}
}
