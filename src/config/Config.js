import _ from 'lodash'

let host = null
const invalidHosts = ['http://undefined', 'http://null', 'https://undefined', 'https://null']

export const setHost = url => {
	host = url
}

export const getHost = () => {
	if ((host || '').startsWith('192') || (host || '').startsWith('localhost')) {
		// return `http://localhost:8080`
		return `http://${host}`
	}
	// return `https://localhost:8080`
	return `https://${host}`
}

export function isValidURL() {
	return _.indexOf(invalidHosts, getHost()) === -1
}
