import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { List } from 'antd-mobile'
import { connect } from 'react-redux'
import { Button, RouteActions } from '../layout'

@connect()
export default class Dashboard extends Component {
	render() {
		return (
			<View style={{ backgroundColor: 'white', flex: 1 }}>
				<View
					style={{
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Button
						title={'Scan Signature QR'}
						onPress={() =>
							RouteActions.navigateTo(
								this.props.screenProps.rootNavigation,
								'QrSignatureCamera'
							)
						}
					/>
				</View>
			</View>
		)
	}
}
