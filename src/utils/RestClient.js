import { AsyncStorage } from 'react-native'
import { Toast } from 'antd-mobile'
import cookie from 'cookie'
import { NavigationActions } from 'react-navigation'
import axios from 'axios'
import Promise from 'bluebird'
import _ from 'lodash'

const resetActionToAuth = NavigationActions.reset({
	index: 0,
	actions: [NavigationActions.navigate({ routeName: 'auth' })],
})

let cookieCacheInit = null

let store = null

function dispatch() {
	if (store == null) {
		return () => {}
	}

	return store.dispatch
}

export const registerStore = storeParam => {
	store = storeParam
}

const defaultConfig = {
	headers: { 'X-Requested-With': 'XMLHttpRequest' },
	xsrfCookieName: 'CSRF-TOKEN',
	xsrfHeaderName: 'X-CSRF-TOKEN',
}

// Add a request interceptor
axios.interceptors.request.use(config => config, error => {})

// Add a response interceptor
axios.interceptors.response.use(
	response => {
		let cookieLine = response.headers['set-cookie'] || ''
		cookieLine = cookieLine.replace(' ', ';')

		if (cookieLine && cookieLine.length > 0) {
			const cookies = cookie.parse(cookieLine)
			getCookieCache().then(cookieCache => {
				const finalCookies = _.merge(cookieCache, cookies)
				cookieCacheInit = finalCookies
				AsyncStorage.setItem('cookieCache', JSON.stringify(finalCookies))
			})
		}

		return response
	},
	error => {
		if (
			_.get(error, 'response.status', 0) == 403 ||
			_.get(error, 'response.status', '') == '403'
		) {
			Toast.fail('Session Expired', 3, dispatch()(resetActionToAuth), mask)
		}

		return Promise.reject(error)
	}
)

async function getCookieCache() {
	const value = await AsyncStorage.getItem('cookieCache')
	if (value !== null) {
		return JSON.parse(value)
	}
}

export const get = (path, config) =>
	new Promise((resolve, reject) => {
		getCookieCache()
			.then(cookieCache => {
				if (!config) config = {}

				config = _.assign({}, defaultConfig, config)
				config.headers = config.headers || {}
				config.headers['X-Requested-With'] = 'XMLHttpRequest'

				if (cookieCache) {
					config.headers.SESSION = cookieCache.SESSION || ''
					config.headers.Cookie = `SESSION=${config.headers.SESSION}`
				}

				axios
					.get(path, config)
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
			.catch(error => {
				reject(error)
			})
	})

export const post = (path, body, config) =>
	new Promise((resolve, reject) => {
		getCookieCache()
			.then(cookieCache => {
				if (!config) config = {}

				config = _.assign({}, defaultConfig, config)

				config.headers = config.headers || {}
				config.headers['X-Requested-With'] = 'XMLHttpRequest'

				cookieStored = cookieCache || cookieCacheInit

				let session = null
				// let sessionPath = cookieStored['Path'];
				_.map(cookieStored, (val, idx) => {
					if (val.indexOf('SESSION') > -1) {
						sessionArr = val.split('SESSION=')
						session = sessionArr.pop()
					}
				})

				config.headers.SESSION = session || cookieStored.SESSION || ''
				config.headers['X-CSRF-TOKEN'] =
					cookieStored['HttpOnly, CSRF-TOKEN'] || cookieStored['CSRF-TOKEN'] || ''
				// config.headers['Cookie']= 'X-CSRF-TOKEN='+config.headers['X-CSRF-TOKEN']+';SESSION='+config.headers['SESSION'];
				config.headers.Cookie = `SESSION=${config.headers.SESSION}; CSRF-TOKEN=${
					config.headers['X-CSRF-TOKEN']
				}`

				console.log('post config', config)

				axios
					.post(path, body || {}, config)
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
			.catch(error => {
				reject(error)
			})
	})

export const put = (path, body, config) =>
	new Promise((resolve, reject) => {
		getCookieCache()
			.then(cookieCache => {
				if (!config) config = {}

				config = _.assign({}, defaultConfig, config)
				config.headers = config.headers || {}
				config.headers['X-Requested-With'] = 'XMLHttpRequest'

				if (cookieCache) {
					config.headers.SESSION = cookieCache.SESSION || ''
					config.headers['X-CSRF-TOKEN'] =
						cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || ''
					config.headers.Cookie = `X-CSRF-TOKEN=${
						config.headers['X-CSRF-TOKEN']
					};SESSION=${config.headers.SESSION}`
				}

				axios
					.put(path, body || {}, config)
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
			.catch(error => {
				reject(error)
			})
	})

export const patch = (path, body, config) => {
	const _config = config
	return new Promise((resolve, reject) => {
		getCookieCache()
			.then(cookieCache => {
				if (!config) config = {}

				config = _.assign({}, defaultConfig, config)
				config.headers = config.headers || {}
				config.headers['X-Requested-With'] = 'XMLHttpRequest'

				if (cookieCache) {
					config.headers.SESSION = cookieCache.SESSION || ''
					config.headers['X-CSRF-TOKEN'] =
						cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || ''
					config.headers.Cookie = `X-CSRF-TOKEN=${
						config.headers['X-CSRF-TOKEN']
					};SESSION=${config.headers.SESSION}`
				}

				axios
					.patch(path, body || {}, config)
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
			.catch(error => {
				reject(error)
			})
	})
}

export const _delete = (path, config) =>
	new Promise((resolve, reject) => {
		getCookieCache()
			.then(cookieCache => {
				if (!config) config = {}

				config = _.assign({}, defaultConfig, config)
				config.headers = config.headers || {}
				config.headers['X-Requested-With'] = 'XMLHttpRequest'

				if (cookieCache) {
					config.headers.SESSION = cookieCache.SESSION || ''
					config.headers.Cookie = `SESSION=${config.headers.SESSION}`
				}

				axios
					.delete(path, config)
					.then(response => {
						resolve(response)
					})
					.catch(error => {
						reject(error)
					})
			})
			.catch(error => {
				reject(error)
			})
	})
